import 'package:flutter/material.dart';
import 'package:revolt_app/mainStyle.dart';
import 'package:revolt_app/myWidgets/my_buttons.dart';

class SettingsPage extends StatefulWidget {
  const SettingsPage({super.key});

  @override
  State<SettingsPage> createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: greyColor,
        elevation: 1,
        title: Text('Settings'),
      ),
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ListTile(
              onTap: () {},
              leading: Icon(
                Icons.person_outline_rounded,
                color: secondColor,
              ),
              title: Text(
                'Edit Profile',
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
              subtitle: Text('Username'),
            ),
            ListTile(
              onTap: () {},
              leading: Icon(
                Icons.email_outlined,
                color: secondColor,
              ),
              title: Text(
                'Change Email',
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
              subtitle: Text(
                'UserEmail@mail.com',
              ),
            ),
            ListTile(
              onTap: () {},
              leading: Icon(
                Icons.local_phone_outlined,
                color: secondColor,
              ),
              title: Text(
                'Change Phone Number',
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
              subtitle: Text('089698989112'),
            ),
            ListTile(
              onTap: () {},
              leading: Icon(
                Icons.password_outlined,
                color: secondColor,
              ),
              title: Text(
                'Change Password',
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
              subtitle: Text('* * * * * * *'),
            ),
            ListTile(
              onTap: () {},
              leading: Icon(
                Icons.info_outline,
                color: secondColor,
              ),
              title: Text(
                'About Us',
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
            ),
            ListTile(
              onTap: () {},
              leading: Icon(
                Icons.help_outline,
                color: secondColor,
              ),
              title: Text(
                'Help Center',
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 15),
              width: double.infinity,
              child: LineButton(
                text: 'Create a new account',
                onPressed: () {},
              ),
            ),
            spaceLine10,
            Container(
              margin: EdgeInsets.symmetric(horizontal: 15),
              width: double.infinity,
              child: LineButton(
                text: 'Switch account',
                onPressed: () {},
              ),
            ),
          ],
        ),
      ),
    );
  }
}
