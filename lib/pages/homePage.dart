import 'dart:io';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:revolt_app/dumy.dart';
import 'package:revolt_app/mainStyle.dart';
import 'package:revolt_app/myWidgets/my_buttons.dart';
import 'package:revolt_app/myWidgets/my_card.dart';
import 'package:revolt_app/myWidgets/my_drawer.dart';
import 'package:revolt_app/pages/profilePage.dart';
import 'package:scroll_snap_list/scroll_snap_list.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  bool _showAppbar = false;
  ScrollController _scrollController = new ScrollController();
  ScrollController sheetScrollController = ScrollController();
  bool sheetScroll = false;
  bool isScrollingDown = false;
  bool _show = true;
  var _index = 0;
  bool isWhiteAppBar = false;

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    myScroll();
    scrollSheet();
  }

  @override
  void dispose() {
    super.dispose();
    _scrollController.removeListener(() {});
    super.dispose();
  }

  void showBottomBar() {
    setState(() {
      _show = true;
    });
  }

  void hideBottomBar() {
    setState(() {
      _show = false;
    });
  }

  void myScroll() async {
    _scrollController.addListener(() {
      print(_scrollController.offset);
      if (_scrollController.offset >= 40
          // _scrollController.position.userScrollDirection ==
          //   ScrollDirection.reverse
          ) {
        isScrollingDown = false;
        _showAppbar = true;
        showBottomBar();
      } else if (_scrollController.offset <= 40) {
        isScrollingDown = true;
        _showAppbar = false;
        hideBottomBar();
      }

      if (_scrollController.offset >= 280) {
        setState(() {
          isWhiteAppBar = true;
        });
        print('white $isWhiteAppBar');
      } else if (_scrollController.offset <= 300) {
        setState(() {
          isWhiteAppBar = false;
        });
        print('nope $isWhiteAppBar');
      }
    });
  }

  void scrollSheet() {
    sheetScrollController.addListener(() {
      if (sheetScrollController.offset >= 50) {
        setState(() {
          sheetScroll = true;
          print('isscrolllll');
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        exit(0);
      },
      child: SafeArea(
        child: GestureDetector(
          onHorizontalDragEnd: (details) =>
              _scaffoldKey.currentState!.openDrawer(),
          child: Scaffold(
              key: _scaffoldKey,
              backgroundColor: greyColor,
              drawer: MyDrawer(),
              endDrawer: MyDrawer(),
              drawerEnableOpenDragGesture: true,
              endDrawerEnableOpenDragGesture: true,
              drawerDragStartBehavior: DragStartBehavior.start,
              body: Stack(
                children: [
                  SingleChildScrollView(
                    controller: _scrollController,
                    physics: BouncingScrollPhysics(),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: double.infinity,
                          height: 350,
                          margin: const EdgeInsets.all(10),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(14),
                            color: secondColor,
                          ),
                          child: Column(
                            children: [
                              spaceLine10,
                              subAppBar(),
                              spaceLine15,
                              Padding(
                                padding: EdgeInsets.symmetric(horizontal: 10),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Expanded(
                                        // width: MediaQuery.of(context).size.width * 0.40,
                                        child: Text(
                                      'Revolt is More than just english lesson',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 30,
                                          fontWeight: FontWeight.w500),
                                    )),
                                    Expanded(
                                      // margin: EdgeInsets.only(top: 30),
                                      // width: MediaQuery.of(context).size.width * 0.44,
                                      // height: MediaQuery.of(context).size.width * 0.44,
                                      child: Image.asset(
                                        'assets/dumy1.png',
                                        fit: BoxFit.fill,
                                      ),
                                    )
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(10),
                          child: Text(
                            'Your Training',
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 18),
                          ),
                        ),
                        spaceLine10,
                        SizedBox(
                          height: 250,
                          child: PageView.builder(
                            padEnds: false,
                            itemCount: 3,
                            // shrinkWrap: true,
                            controller: PageController(
                              viewportFraction: 0.5,
                            ),
                            scrollDirection: Axis.horizontal,
                            onPageChanged: (int index) => setState(() {
                              _index = index;
                            }),
                            // physics: BouncingScrollPhysics(),
                            // padding: EdgeInsets.symmetric(horizontal: 10),
                            itemBuilder: (context, index) => AnimatedContainer(
                              margin: index == _index
                                  ? EdgeInsets.only(left: 10)
                                  : null,
                              duration: Duration(milliseconds: 200),
                              child: Transform.scale(
                                scale: index == _index ? 1 : 0.9,
                                child: MyCard(
                                  category: trainingList[index]['category'],
                                  name: trainingList[index]['name'],
                                  status: 'sedang menunggu',
                                ),
                              ),
                            ),
                            // itemSize: 100,
                            // onItemFocus: (int) {},
                          ),
                        ),
                        spaceLine10,
                        Padding(
                          padding: const EdgeInsets.all(10),
                          child: Text(
                            'Trainings',
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 18),
                          ),
                        ),
                        spaceLine10,
                        SizedBox(
                          height: 300,
                          child: ListView.builder(
                            itemCount: trainingList.length,
                            shrinkWrap: true,
                            scrollDirection: Axis.horizontal,
                            physics: BouncingScrollPhysics(),
                            padding: EdgeInsets.symmetric(horizontal: 10),
                            itemBuilder: (context, index) => MyCard(
                              category: trainingList[index]['category'],
                              name: trainingList[index]['name'],
                              description: trainingList[index]['description'],
                              price: trainingList[index]['price'],
                              diskon: trainingList[index]['diskon'],
                              onpressed: () {
                                return joinBottomSheet(trainingList[index]);
                              },
                            ),
                          ),
                        ),
                        spaceLine15,
                        Container(
                          width: double.infinity,
                          height: 200,
                          margin: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                              color: secondColor,
                              borderRadius: BorderRadius.circular(14)),
                          // child: ,
                        ),
                        spaceLine15,
                        spaceLine15,
                        spaceLine15,
                        spaceLine15,
                      ],
                    ),
                  ),
                  _showAppbar == false
                      ? Container()
                      : Align(
                          alignment: Alignment.topCenter,
                          child: AnimatedContainer(
                              // transform: ,
                              duration: Duration(seconds: 3),
                              // margin: EdgeInsets.all(8),
                              padding: EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                  // borderRadius: BorderRadius.circular(14),
                                  boxShadow: [
                                    BoxShadow(
                                        color: isWhiteAppBar
                                            ? Colors.grey.shade300
                                            : Colors.transparent,
                                        offset: Offset(0, 3),
                                        blurRadius: 3,
                                        spreadRadius: 3)
                                  ],
                                  color:
                                      isWhiteAppBar ? greyColor : secondColor),
                              child: _showAppbar == false
                                  ? Container()
                                  : subAppBar()),
                        )
                ],
              )),
        ),
      ),
    );
  }

  joinBottomSheet(var packet) {
    return showModalBottomSheet(
      barrierColor: Colors.grey.shade50.withOpacity(0.20),
      elevation: 5,
      backgroundColor: Colors.white,
      isScrollControlled: true,
      useRootNavigator: true,
      constraints: BoxConstraints(
          minHeight: MediaQuery.of(context).size.height * 0.50,
          maxHeight: MediaQuery.of(context).size.height * 0.70),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(14), topRight: Radius.circular(14))),
      context: context,
      builder: (context) => ClipRRect(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(14), topRight: Radius.circular(14)),
        child: Stack(
          children: [
            SingleChildScrollView(
              controller: sheetScrollController,
              physics: BouncingScrollPhysics(),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: [
                  SizedBox(
                    height: 50,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: Text(
                      packet['name'],
                      style:
                          TextStyle(fontSize: 19, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: Text(
                      packet['category'],
                      style: TextStyle(color: Colors.grey),
                    ),
                  ),
                  spaceLine10,
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: Text(
                      packet['description'],
                    ),
                  ),
                  spaceLine15,
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: Text(
                      'Ulasan',
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                  ),
                  spaceLine15,
                  ListView.builder(
                    itemCount: 14,
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemBuilder: (context, index) => ListTile(
                      leading: CircleAvatar(backgroundColor: secondColor),
                      title: Text('Username'),
                      subtitle: Text('Komentar akan muncul disisni'),
                    ),
                  ),
                  SizedBox(
                    height: 60,
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.topCenter,
              child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(14),
                        topRight: Radius.circular(14)),
                    color: Colors.white),
                padding: const EdgeInsets.all(10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Join',
                      style: TextStyle(
                          color: mainColor,
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                    ),
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          sheetScroll = false;
                        });
                        Navigator.pop(context);
                      },
                      child: Icon(
                        Icons.close,
                        color: secondColor,
                      ),
                    )
                  ],
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                width: double.infinity,
                color: Colors.white,
                padding: EdgeInsets.all(10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                        child: Text(
                      "Rp${packet['price']}",
                      style: TextStyle(
                          color: Colors.green,
                          fontWeight: FontWeight.bold,
                          fontSize: 18),
                    )),
                    Expanded(
                      flex: 4,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: PasifButtos(
                              text: 'Cart',
                              onPressed: () {},
                            ),
                          ),
                          SizedBox(
                            width: 8,
                          ),
                          Expanded(
                            child: ActiveButton(
                              text: 'Join',
                              onPressed: () {},
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  subAppBar() {
    return ListTile(
      title: GestureDetector(
        onTap: () {
          _scaffoldKey.currentState?.openDrawer();
        },
        child: Text(
          'r e v o l t',
          style: TextStyle(
              color: isWhiteAppBar ? secondColor : Colors.white,
              fontSize: 25,
              fontWeight: FontWeight.bold),
        ),
      ),
      trailing: InkWell(
          onTap: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => ProfilePage(),
              )),
          child: CircleAvatar(
              backgroundColor: isWhiteAppBar ? mainColor : pointViewColor)),
    );
  }
}
