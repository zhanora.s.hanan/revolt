import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:revolt_app/dumy.dart';
import 'package:revolt_app/mainStyle.dart';
import 'package:revolt_app/myWidgets/my_buttons.dart';

class CartPage extends StatefulWidget {
  const CartPage({super.key});

  @override
  State<CartPage> createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  ScrollController _scrollController = ScrollController();
  bool showBottomAppbar = true;
  var selectedPacket;
  bool selectedItemPacket = false;

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      print(_scrollController.offset);
      if (_scrollController.position.userScrollDirection ==
          ScrollDirection.reverse) {
        setState(() {
          showBottomAppbar = false;
        });
      } else if (_scrollController.position.userScrollDirection ==
          ScrollDirection.forward) {
        setState(() {
          showBottomAppbar = true;
        });
      } else if (_scrollController.offset >= 888) {
        setState(() {
          showBottomAppbar = true;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: greyColor,
      appBar: AppBar(
        backgroundColor: greyColor,
        elevation: 1,
        title: Text('Cart'),
      ),
      body: SingleChildScrollView(
        controller: _scrollController,
        physics: BouncingScrollPhysics(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: [
            // spaceLine10,
            // Text(
            //   'Your Cart',
            //   style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            // ),
            ListView.builder(
                itemCount: 20,
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  return Slidable(
                    startActionPane: ActionPane(
                        motion: ScrollMotion(),
                        extentRatio: 0.25,
                        children: [
                          SlidableAction(
                            onPressed: (context) {},
                            backgroundColor: Colors.blueAccent,
                            foregroundColor: Colors.white,
                            icon: Icons.share,
                          )
                        ]),
                    endActionPane: ActionPane(
                        motion: ScrollMotion(),
                        extentRatio: 0.25,
                        children: [
                          SlidableAction(
                            onPressed: (context) {},
                            backgroundColor: Colors.redAccent,
                            foregroundColor: Colors.white,
                            icon: Icons.close_rounded,
                          )
                        ]),
                    child: GestureDetector(
                      onLongPress: () {
                        setState(() {
                          selectedPacket = index;
                          selectedItemPacket = true;
                        });
                      },
                      onTap: selectedPacket == null
                          ? () {}
                          : () {
                              setState(() {
                                // selectedPacket = index;
                                selectedItemPacket = true;
                              });
                            },
                      child: AnimatedContainer(
                        duration: Duration(milliseconds: 300),
                        padding:
                            EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                        decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                  color: Colors.grey.shade300, width: 1.5)),
                        ),
                        child: Row(
                          children: [
                            selectedPacket == index
                                ? IconButton(
                                    onPressed: () {
                                      // selectedItemPacket = false;
                                      selectedPacket = null;
                                      setState(() {});
                                    },
                                    icon: Icon(
                                      selectedPacket != null
                                          ? Icons.check_box_rounded
                                          : Icons
                                              .check_box_outline_blank_rounded,
                                      color: pointViewColor,
                                      size: 35,
                                    ))
                                : Container(),
                            Container(
                              width: 70,
                              height: 70,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(7),
                                color: mainColor,
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Expanded(
                              flex: 2,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  // Text(index.toString()),
                                  Text(
                                    // trainingList[index]['category'],
                                    'category',
                                    style: TextStyle(color: Colors.grey),
                                  ),
                                  Text(
                                    // trainingList[index]['name'],
                                    'name packet',
                                    style: TextStyle(
                                        fontSize: 17,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    // trainingList[index]['category'],
                                    'type',
                                    style: TextStyle(color: Colors.grey),
                                  ),
                                ],
                              ),
                            ),
                            Expanded(
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  Text(
                                    'Price',
                                    style: TextStyle(
                                        color: Colors.green,
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    'diskon',
                                    style: TextStyle(
                                      decoration: TextDecoration.lineThrough,
                                    ),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  );
                })
          ],
        ),
      ),
      bottomNavigationBar: AnimatedContainer(
        duration: Duration(seconds: 5),
        child: Visibility(
          visible: showBottomAppbar,
          child: Container(
            padding: EdgeInsets.all(10),
            height: showBottomAppbar ? 70 : 0,
            color: Colors.white,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Total',
                        style: TextStyle(color: Colors.grey),
                      ),
                      Text(
                        'Rp.000.000',
                        style: TextStyle(
                            color: pointViewColor,
                            fontSize: 16,
                            fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: selectedPacket == null
                      ? PasifButtos(
                          text: 'Checkout',
                        )
                      : ActiveButton(
                          text: 'Checkout',
                          onPressed: () {},
                        ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
