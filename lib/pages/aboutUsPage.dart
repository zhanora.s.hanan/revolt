import 'package:auto_animated/auto_animated.dart';
import 'package:flutter/material.dart';
import 'package:revolt_app/mainStyle.dart';

class AboutUs extends StatefulWidget {
  const AboutUs({super.key});

  @override
  State<AboutUs> createState() => _AboutUsState();
}

class _AboutUsState extends State<AboutUs> {
  ScrollController _scrollController = ScrollController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('About Us'),
          elevation: 0,
          backgroundColor: greyColor,
        ),
        body: CustomScrollView(
          controller: _scrollController,
          slivers: [
            LiveSliverList(
              controller: _scrollController,
              itemCount: 11,
              showItemDuration: Duration(seconds: 3),
              // shrinkWrap: true,
              // physics: BouncingScrollPhysics(),
              itemBuilder: (context, index, animation) {
                bool rtl = index % 2 == 0 ? false : true;
                print(index);
                return index == 0
                    ? Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            FadeTransition(
                              opacity: Tween<double>(
                                begin: 0,
                                end: 1,
                              ).animate(animation),
                              // And slide transition
                              child: SlideTransition(
                                position: Tween<Offset>(
                                  begin: Offset(0, -0.1),
                                  end: Offset.zero,
                                ).animate(animation),
                                child: Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.50,
                                  height:
                                      MediaQuery.of(context).size.width * 0.50,
                                  // color: mainColor,
                                  child: Image.asset('assets/dumy1.png'),
                                ),
                              ),
                            ),
                            FadeTransition(
                              opacity: Tween<double>(
                                begin: 0,
                                end: 1,
                              ).animate(animation),
                              // And slide transition
                              child: SlideTransition(
                                position: Tween<Offset>(
                                  begin: Offset(0, -0.1),
                                  end: Offset.zero,
                                ).animate(animation),
                                child: Container(
                                  width: MediaQuery.of(context).size.width,
                                  // height: MediaQuery.of(context).size.width * 0.50,
                                  // color: pointViewColor,
                                  child: Text(
                                    'Revolt is more than just english lesson',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 25,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      )
                    : index == 10
                        ? Padding(
                            padding: const EdgeInsets.symmetric(vertical: 10),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                FadeTransition(
                                  opacity: Tween<double>(
                                    begin: 0,
                                    end: 1,
                                  ).animate(animation),
                                  // And slide transition
                                  child: SlideTransition(
                                    position: Tween<Offset>(
                                      begin: Offset(0, -0.1),
                                      end: Offset.zero,
                                    ).animate(animation),
                                    child: Container(
                                      width: MediaQuery.of(context).size.width *
                                          0.50,
                                      height:
                                          MediaQuery.of(context).size.width *
                                              0.50,
                                      // color: mainColor,
                                      child: Image.asset('assets/dumy1.png'),
                                    ),
                                  ),
                                ),
                                FadeTransition(
                                  opacity: Tween<double>(
                                    begin: 0,
                                    end: 1,
                                  ).animate(animation),
                                  // And slide transition
                                  child: SlideTransition(
                                    position: Tween<Offset>(
                                      begin: Offset(0, -0.1),
                                      end: Offset.zero,
                                    ).animate(animation),
                                    child: Container(
                                      width: MediaQuery.of(context).size.width,
                                      // height:
                                      //     MediaQuery.of(context).size.width * 0.50,
                                      // color: pointViewColor,
                                      child: Text(
                                        'r e v o l t',
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          )
                        : rtl
                            ? Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 10),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    FadeTransition(
                                      opacity: Tween<double>(
                                        begin: 0,
                                        end: 1,
                                      ).animate(animation),
                                      // And slide transition
                                      child: SlideTransition(
                                        position: Tween<Offset>(
                                          begin: Offset(0, -0.1),
                                          end: Offset.zero,
                                        ).animate(animation),
                                        child: Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.50,
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.50,
                                          // color: mainColor,
                                          child:
                                              Image.asset('assets/dumy1.png'),
                                        ),
                                      ),
                                    ),
                                    FadeTransition(
                                      opacity: Tween<double>(
                                        begin: 0,
                                        end: 1,
                                      ).animate(animation),
                                      // And slide transition
                                      child: SlideTransition(
                                        position: Tween<Offset>(
                                          begin: Offset(0, -0.1),
                                          end: Offset.zero,
                                        ).animate(animation),
                                        child: Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.50,
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.50,
                                          // color: pointViewColor,
                                          child: Text(
                                            'Revolt is more than just english lesson',
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                                fontSize: 25,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              )
                            : Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 10),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    FadeTransition(
                                      opacity: Tween<double>(
                                        begin: 0,
                                        end: 1,
                                      ).animate(animation),
                                      // And slide transition
                                      child: SlideTransition(
                                        position: Tween<Offset>(
                                          begin: Offset(0, -0.1),
                                          end: Offset.zero,
                                        ).animate(animation),
                                        child: Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.50,
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.50,
                                          // color: pointViewColor,
                                          child: Text(
                                            'Revolt is more than just english lesson',
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                                fontSize: 25,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                      ),
                                    ),
                                    FadeTransition(
                                      opacity: Tween<double>(
                                        begin: 0,
                                        end: 1,
                                      ).animate(animation),
                                      // And slide transition
                                      child: SlideTransition(
                                        position: Tween<Offset>(
                                          begin: Offset(0, -0.1),
                                          end: Offset.zero,
                                        ).animate(animation),
                                        child: Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.50,
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.50,
                                          // color: mainColor,
                                          child:
                                              Image.asset('assets/dumy1.png'),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              );
              },
            ),
          ],
        ));
  }
}
