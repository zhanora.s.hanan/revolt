import 'package:flutter/material.dart';
import 'package:revolt_app/mainStyle.dart';
import 'package:revolt_app/myWidgets/my_card.dart';
import 'package:revolt_app/myWidgets/my_listile.dart';
import 'package:revolt_app/myWidgets/my_sheets.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({super.key});

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  int _index = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: greyColor,
      appBar: AppBar(
        backgroundColor: greyColor,
        elevation: 1,
        title: Text('Your Profile'),
        actions: [
          IconButton(
              onPressed: () {
                showModalBottomSheet(
                  context: context,
                  constraints: BoxConstraints(
                      maxHeight: MediaQuery.of(context).size.height * 0.90),
                  backgroundColor: Colors.transparent,
                  builder: (context) {
                    return Padding(
                      padding: const EdgeInsets.all(14),
                      child: AccountSheets(),
                    );
                  },
                );
              },
              icon: Icon(Icons.more_vert))
        ],
      ),
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: [
            Padding(
              padding: const EdgeInsets.all(10),
              child: Row(
                children: [
                  SizedBox(
                    height: MediaQuery.of(context).size.width * 0.30,
                    width: MediaQuery.of(context).size.width * 0.30,
                    child: CircleAvatar(backgroundColor: pointViewColor),
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Budi Raja',
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold),
                        ),
                        Text(
                          'budiRaja@gmail.com',
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(color: Colors.grey, fontSize: 15),
                        ),
                        Text(
                          '089698989112',
                          style: TextStyle(color: Colors.grey, fontSize: 15),
                        ),
                        RichText(
                            text: TextSpan(children: [
                          TextSpan(
                              text: '0',
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  color: secondColor),
                              children: [
                                TextSpan(
                                    text: ' Training ',
                                    style: TextStyle(color: Colors.grey)),
                              ]),
                          TextSpan(
                              text: '0',
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  color: secondColor),
                              children: [
                                TextSpan(
                                    text: ' Moduls',
                                    style: TextStyle(color: Colors.grey)),
                              ]),
                        ])),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10),
              child: Text(
                'Your Training',
                textAlign: TextAlign.start,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
              ),
            ),
            spaceLine10,
            SizedBox(
              height: 250,
              child: PageView.builder(
                padEnds: false,
                itemCount: 4,
                // shrinkWrap: true,
                controller: PageController(
                  viewportFraction: 0.5,
                ),
                scrollDirection: Axis.horizontal,
                onPageChanged: (int index) => setState(() {
                  _index = index;
                }),
                // physics: BouncingScrollPhysics(),
                // padding: EdgeInsets.symmetric(horizontal: 10),
                itemBuilder: (context, index) => AnimatedContainer(
                  margin: index == _index ? EdgeInsets.only(left: 10) : null,
                  duration: Duration(milliseconds: 200),
                  child: Transform.scale(
                    scale: index == _index ? 1 : 0.9,
                    child: MyCard(
                      category: 'english',
                      name: 'Super Camp',
                      status: 'sedang menunggu',
                    ),
                  ),
                ),
                // itemSize: 100,
                // onItemFocus: (int) {},
              ),
            ),
            spaceLine10,
            Padding(
              padding: const EdgeInsets.all(10),
              child: Text(
                'Your Modul',
                textAlign: TextAlign.start,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
              ),
            ),
            spaceLine10,
            ListView.builder(
              itemCount: 12,
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemBuilder: (context, index) {
                return MyListTile(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  title: 'Modul $index',
                  subtitle: 'english sentence',
                );
              },
            )
          ],
        ),
      ),
    );
  }
}
