import 'package:flutter/material.dart';

List<Map<String, dynamic>> trainingList = [
  {
    'image': '',
    'category': 'english',
    'name': 'Revolt Super Camp',
    'description':
        'Ini adalah pelatihan bagi anda yang ingin menguasai bahasi inggris dalam waktu singkat. Pelatihan ini adalah pelatihan terbaik yang kami miliki . Pelatihan ini akan di laksanakan offline selama kurang lebih satu minggu di waktu yang sudah di tentukan',
    'price': '14jt',
    'diskon': '15jt'
  },
  {
    'image': '',
    'category': 'English',
    'name': 'Revolt Reguler packet',
    'description':
        'Ini adalah paket pelatihan terbaik abgi anda yang ingin mengausai bahasa inggris dalam waktu cepat',
    'price': '6jt',
    'diskon': '7jt'
  },
  {
    'image': '',
    'category': 'English ',
    'name': 'Packet Module',
    'description':
        'Ini adalah paket yang sangat cocok untuk kamu yang suka belajar otodidak dan mau explore dengan kemampuan sendiri dan mau terus berusaha hingga bisa',
    'price': '250rb',
    'diskon': '199rb'
  },
  {
    'image': '',
    'category': 'English',
    'name': 'Packet Module dasar',
    'description':
        'Ini adalah packet yang sangat cocok untuk kamu yang lagi sekolah. packet ini Berisi modul modul bahasa inggris yang mana metode di dalam nya sudah sangat terbukti membuat banyak org berhasil menguasai bahasa inggris',
    'price': '60rb',
    'diskon': '70rb'
  },
];
