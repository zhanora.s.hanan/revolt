import 'package:flutter/material.dart';

var mainColor = const Color(0xff82ADB4);
var secondColor = const Color(0xff292C35);
var pointViewColor = const Color(0xffEFBB3D);
var compColor = const Color(0xffD09B7C);
var greyColor = const Color(0xffF9F9F9);

var spaceLine5 = const SizedBox(
  height: 5,
);
var spaceLine10 = const SizedBox(
  height: 10,
);
var spaceLine15 = const SizedBox(
  height: 15,
);
