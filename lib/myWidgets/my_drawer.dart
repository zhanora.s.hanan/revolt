import 'package:flutter/material.dart';
import 'package:revolt_app/mainStyle.dart';
import 'package:revolt_app/myWidgets/my_dialogs.dart';
import 'package:revolt_app/myWidgets/my_sheets.dart';
import 'package:revolt_app/pages/aboutUsPage.dart';
import 'package:revolt_app/pages/cartPage.dart';
import 'package:revolt_app/pages/profilePage.dart';
import 'package:revolt_app/pages/settingsPage.dart';

class MyDrawer extends StatefulWidget {
  const MyDrawer({super.key});

  @override
  State<MyDrawer> createState() => _MyDrawerState();
}

class _MyDrawerState extends State<MyDrawer> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      backgroundColor: Colors.white,
      elevation: 3,
      width: MediaQuery.of(context).size.width * 0.90,
      shape: RoundedRectangleBorder(),
      child: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                spaceLine10,
                ListTile(
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => ProfilePage(),
                        ));
                  },
                  // minLeadingWidth: 80,
                  leading: SizedBox(
                      height: 60,
                      width: 60,
                      child: CircleAvatar(backgroundColor: secondColor)),
                  // trailing: IconButton(
                  //   onPressed: () {
                  //     Navigator.pop(context);
                  //   },
                  //   icon: Icon(Icons.close),
                  // ),
                  trailing: IconButton(
                    onPressed: () {
                      Navigator.pop(context);
                      showModalBottomSheet(
                        context: context,
                        // barrierColor: Colors.grey.shade300.withOpacity(0.20),
                        backgroundColor: Colors.transparent,
                        builder: (context) {
                          return Padding(
                            padding: const EdgeInsets.all(14),
                            child: AccountSheets(),
                          );
                        },
                      );
                    },
                    icon: Icon(Icons.more_vert_rounded),
                  ),
                ),
                ListTile(
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => ProfilePage(),
                        ));
                  },
                  title: Text(
                    'Username',
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                  subtitle: Text(
                    'Full Name',
                    style: TextStyle(color: Colors.grey),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: RichText(
                      text: TextSpan(children: [
                    TextSpan(
                        text: '0',
                        style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.bold,
                            color: secondColor),
                        children: [
                          TextSpan(
                              text: ' Training ',
                              style: TextStyle(color: Colors.grey)),
                        ]),
                    TextSpan(
                        text: '0',
                        style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.bold,
                            color: secondColor),
                        children: [
                          TextSpan(
                              text: ' Moduls',
                              style: TextStyle(color: Colors.grey)),
                        ]),
                  ])),
                ),
                spaceLine10,
                spaceLine10,
                ListTile(
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => ProfilePage(),
                        ));
                  },
                  leading: Icon(
                    Icons.person_outline,
                    color: secondColor,
                  ),
                  title: Text(
                    'Profile',
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                ),
                ListTile(
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => CartPage(),
                        ));
                  },
                  leading: Icon(
                    Icons.shopping_bag_outlined,
                    color: secondColor,
                  ),
                  title: Text(
                    'Cart',
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                ),
                // ListTile(
                //   onTap: () {},
                //   leading: Icon(
                //     Icons.favorite_border,
                //     color: secondColor,
                //   ),
                //   title: Text(
                //     'Favorite',
                //     style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                //   ),
                // ),
                ListTile(
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => AboutUs(),
                        ));
                  },
                  leading: Icon(
                    Icons.info_outline,
                    color: secondColor,
                  ),
                  title: Text(
                    'About Us',
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                ),
                ListTile(
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => SettingsPage(),
                        ));
                  },
                  leading: Icon(
                    Icons.settings_outlined,
                    color: secondColor,
                  ),
                  title: Text(
                    'Settings',
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              width: double.infinity,
              padding: EdgeInsets.all(15),
              color: Colors.white.withOpacity(0.10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(onPressed: () {}, icon: Icon(Icons.help_outline)),
                  IconButton(
                      onPressed: () {
                        Navigator.pop(context);
                        showDialog(
                          context: context,
                          builder: (context) => LogOutDialog(),
                        );
                      },
                      icon: Icon(Icons.exit_to_app_rounded))
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
