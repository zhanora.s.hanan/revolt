import 'package:flutter/material.dart';
import 'package:revolt_app/mainStyle.dart';

class MyListTile extends StatelessWidget {
  Function()? onTap;
  String? title;
  String? subtitle;
  double marginHori;
  double marginVert;
  MyListTile(
      {super.key,
      this.onTap,
      this.title,
      this.subtitle,
      this.marginHori = 10,
      this.marginVert = 5});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.symmetric(horizontal: marginHori, vertical: marginVert),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10),
        child: InkWell(
          onTap: onTap,
          child: Container(
            padding: EdgeInsets.all(14),
            decoration: BoxDecoration(
                border: Border(left: BorderSide(color: mainColor, width: 5)),
                color: Colors.white),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    title == null
                        ? Container()
                        : Text(
                            '$title',
                            style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                    subtitle == null ? Container() : Text('$subtitle'),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
