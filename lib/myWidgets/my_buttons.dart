import 'package:flutter/material.dart';

import '../mainStyle.dart';

class ActiveButton extends StatelessWidget {
  String? text;
  Function()? onPressed;
  ActiveButton({super.key, this.text, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressed,
      child: Text(
        text!,
        style: TextStyle(color: Colors.white),
      ),
      style: ElevatedButton.styleFrom(
          elevation: 0,
          backgroundColor: mainColor,
          disabledBackgroundColor: mainColor,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(14))),
    );
  }
}

class PasifButtos extends StatelessWidget {
  String? text;
  Function()? onPressed;
  PasifButtos({super.key, this.text, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressed,
      child: Text(
        text!,
        // style: TextStyle(color: Colors.white),
      ),
      style: ElevatedButton.styleFrom(
          elevation: 0,
          backgroundColor: Colors.grey[300],
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(14))),
    );
  }
}

class LoadingButton extends StatelessWidget {
  // String? text;
  Function()? onPressed;
  LoadingButton({super.key, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressed,
      child: SizedBox(
          width: 20,
          height: 20,
          child: CircularProgressIndicator(
            color: Colors.white,
            strokeWidth: 3,
          )),
      style: ElevatedButton.styleFrom(
          elevation: 0,
          backgroundColor: Colors.grey[300],
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(14))),
    );
  }
}

class LineButton extends StatelessWidget {
  String? text;
  Function()? onPressed;
  LineButton({super.key, this.text, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressed,
      child: Text(
        text!,
        style: TextStyle(color: mainColor),
      ),
      style: ElevatedButton.styleFrom(
          elevation: 0,
          disabledBackgroundColor: mainColor,
          backgroundColor: Colors.white,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(14),
              side: BorderSide(color: mainColor, width: 3))),
    );
  }
}
