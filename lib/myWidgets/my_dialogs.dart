import 'package:flutter/material.dart';
import 'package:revolt_app/myWidgets/my_buttons.dart';

class LogOutDialog extends StatelessWidget {
  const LogOutDialog({super.key});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(14)),
      backgroundColor: Colors.white,
      actionsAlignment: MainAxisAlignment.center,
      title: Text(
        'Are You Sure\nWant to logout',
        textAlign: TextAlign.center,
      ),
      actions: [
        PasifButtos(
          onPressed: () {
            Navigator.pop(context);
          },
          text: 'Batal',
        ),
        ActiveButton(
          onPressed: () {},
          text: 'Yaa',
        )
      ],
    );
  }
}
