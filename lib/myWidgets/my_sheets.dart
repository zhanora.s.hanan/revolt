import 'package:flutter/material.dart';
import 'package:revolt_app/mainStyle.dart';
import 'package:revolt_app/myWidgets/my_buttons.dart';

class AccountSheets extends StatelessWidget {
  Function()? onClosing;
  AccountSheets({super.key, this.onClosing});

  @override
  Widget build(BuildContext context) {
    return BottomSheet(
      onClosing: () {},
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(14)),
      constraints:
          BoxConstraints(maxHeight: MediaQuery.of(context).size.height * 0.90),
      builder: (context) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            // spaceLine10,
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 8),
              child: Text(
                'Accounts',
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
            ),
            ListTile(
              onTap: () {},
              title: Text('Edit Profile'),
              subtitle: Text('Username'),
            ),
            ListTile(
              onTap: () {},
              title: Text('Change Email'),
              subtitle: Text('UserEmail@mail.com'),
            ),
            ListTile(
              onTap: () {},
              title: Text('Change Phone Number'),
              subtitle: Text('089698989112'),
            ),
            ListTile(
              onTap: () {},
              title: Text('Change Password'),
              subtitle: Text('* * * * * * *'),
            ),
            // SizedBox(
            //   width: double.infinity,
            //   child: LineButton(
            //     text: 'Create a new account',
            //     onPressed: () {},
            //   ),
            // ),
            // spaceLine10,
            // SizedBox(
            //   width: double.infinity,
            //   child: LineButton(
            //     text: 'Switch account',
            //     onPressed: () {},
            //   ),
            // )
          ],
        );
      },
    );
  }
}
