import 'package:flutter/material.dart';
import 'package:revolt_app/myWidgets/my_buttons.dart';

import '../mainStyle.dart';

class MyCard extends StatelessWidget {
  String? image;
  String? category;
  String? name;
  String? description;
  String? price;
  String? diskon;
  Function()? onpressed;
  String? status;
  MyCard(
      {super.key,
      this.image,
      this.category,
      this.name,
      this.description,
      this.price,
      this.diskon,
      this.onpressed,
      this.status});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 10),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(14),
        child: Container(
          width: 200,
          decoration: BoxDecoration(
            color: Colors.white,
          ),
          child: Column(
            children: [
              Container(
                width: double.infinity,
                height: 150,
                color: Colors.grey[300],
                child: image == null ? null : Image.network('$image'),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    category == null
                        ? Container()
                        : Text(
                            '$category',
                            style: TextStyle(color: Colors.grey),
                          ),
                    name == null
                        ? Container()
                        : Text(
                            '$name',
                            maxLines: 1,
                            overflow: TextOverflow.clip,
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 19),
                          ),
                    spaceLine5,
                    description == null
                        ? Container()
                        : Text(
                            '$description',
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                          ),
                    onpressed == null && price == null && diskon == null
                        ? Container()
                        : Align(
                            alignment: Alignment.bottomCenter,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    price == null
                                        ? Container()
                                        : Text(
                                            '$price',
                                            style: TextStyle(
                                                color: Colors.green,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 16),
                                          ),
                                    category == null
                                        ? Container()
                                        : Text(
                                            '$diskon',
                                            style: TextStyle(
                                              decoration:
                                                  TextDecoration.lineThrough,
                                              color: Colors.grey,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                  ],
                                ),
                                onpressed == null
                                    ? Container()
                                    : ActiveButton(
                                        text: 'join',
                                        onPressed: onpressed,
                                      )
                              ],
                            ),
                          ),
                    status == null
                        ? Container()
                        : Container(
                            margin: EdgeInsets.only(top: 8),
                            padding: EdgeInsets.all(5),
                            width: double.infinity,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              color: mainColor,
                            ),
                            child: Center(
                              child: Text(
                                '$status',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
