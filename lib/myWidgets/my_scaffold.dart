import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:revolt_app/mainStyle.dart';
import 'package:revolt_app/myWidgets/my_drawer.dart';

class MyScaffold extends StatelessWidget {
  Key? key;
  AppBar? appBar;
  Widget? body;
  MyScaffold({this.key, this.appBar, this.body});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: key,
      backgroundColor: greyColor,
      drawer: MyDrawer(),
      endDrawer: MyDrawer(),
      drawerEnableOpenDragGesture: true,
      endDrawerEnableOpenDragGesture: true,
      drawerDragStartBehavior: DragStartBehavior.start,
      appBar: appBar,
      body: body,
    );
  }
}
